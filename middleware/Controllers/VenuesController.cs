﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuzzyString;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/Venues")]
    public class VenuesController : SearchableController<Venue>
    {
        private VenueModel db = new VenueModel();

        // GET: api/Venues
        /// <summary>
        /// Get a list of venues.
        /// </summary>
        /// <remarks>Returns a list of all venues.</remarks>
        /// <returns>Venue schema</returns>
        public IQueryable<Venue> GetVenues()
        {
            return db.Venues;
        }

        // GET: api/Venues/5
        /// <summary>
        /// Get a specfiic venue.
        /// </summary>
        /// <remarks>Returns a specific venue</remarks>
        /// <param name="id">Venue ID</param>
        /// <returns>Venue schema</returns>
        [ResponseType(typeof(Venue))]
        public async Task<IHttpActionResult> GetVenue(decimal id)
        {
            Venue venue = await db.Venues.FindAsync(id);
            if (venue == null)
            {
                return NotFound();
            }

            return Ok(venue);
        }

        // PUT: api/Venues/5
        /// <summary>
        /// Update information for a specified venue.
        /// </summary>
        /// <remarks>Modify information for a specified venue.</remarks>
        /// <param name="id">Venue ID</param>
        /// <param name="venue">Venue schema to update</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVenue(decimal id, Venue venue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != venue.VENUEID)
            {
                return BadRequest();
            }

            db.Entry(venue).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VenueExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Venues
        /// <summary>
        /// Add/update information about a venue.
        /// </summary>
        /// <remarks>Update information about a venue, or add if it doesn't exist.</remarks>
        /// <param name="venue">Venue schema</param>
        /// <returns>Venue schema</returns>
        [ResponseType(typeof(Venue))]
        public async Task<IHttpActionResult> PostVenue(Venue venue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (venue.VENUEID == 0)
            {
                venue.VENUEID = db.Venues.Count() + 1;
            }

            db.Venues.Add(venue);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VenueExists(venue.VENUEID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = venue.VENUEID }, venue);
        }

        // DELETE: api/Venues/5
        /// <summary>
        /// Delete a venue with the specified ID.
        /// </summary>
        /// <remarks>Delete a venue with the given ID</remarks>
        /// <param name="id">Venue ID</param>
        /// <returns>Venue schema</returns>
        [ResponseType(typeof(Venue))]
        public async Task<IHttpActionResult> DeleteVenue(decimal id)
        {
            var venue = await db.Venues.FindAsync(id);
            if (venue == null)
            {
                return NotFound();
            }

            db.Venues.Remove(venue);
            await db.SaveChangesAsync();

            return Ok(venue);
        }

        // POST: api/Venues/search
        /// <summary>
        /// Search venues based on a specified query
        /// </summary>
        /// <remarks>Searches venue name for the passed query</remarks>
        /// <param name="q">Query to search for</param>
        /// <returns>Search results</returns>
        [Route("search")]
        [AcceptVerbs("GET")]
        public List<Venue> SearchVenues(string q)
        {
            return SearchByField(db.Venues, q, "VENUENAME");
        }

        [Route("downloadImage")]
        [AcceptVerbs("GET")]
        public async Task<HttpResponseMessage> DownloadImage(decimal id, int width, int height, string type)
        {
            var artist = await db.Venues.FindAsync(id);
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            if (artist.IMAGE == null)
            {
                result.Content = null;
                return result;
            }

            switch (type.ToUpper())
            {
                case "B":
                    result.Content = new ByteArrayContent(artist.IMAGE.IMAGEBANNER);
                    break;
                case "T":
                    result.Content = new ByteArrayContent(artist.IMAGE.IMAGETHUMB);
                    break;
            }
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VenueExists(decimal id)
        {
            return db.Venues.Count(e => e.VENUEID == id) > 0;
        }
    }
}