﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/Genres")]
    public class GenresController : ApiController
    {
        private GenreModel db = new GenreModel();

        // GET: api/Genres
        /// <summary>
        /// Get information for all genres.
        /// </summary>
        /// <remarks>Returns a list of all genres</remarks>
        /// <returns>Genre schema</returns>
        public IQueryable<Genre> GetGenres()
        {
            return db.Genres;
        }

        // GET: api/Genres/5
        /// <summary>
        /// Get information about a specific genre.
        /// </summary>
        /// <remarks>Returns a specific genre</remarks>
        /// <param name="id">Genre ID</param>
        /// <returns>Genre schema</returns>
        [ResponseType(typeof(Genre))]
        public async Task<IHttpActionResult> GetGenre(decimal id)
        {
            Genre genre = await db.Genres.FindAsync(id);
            if (genre == null)
            {
                return NotFound();
            }

            return Ok(genre);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GenreExists(decimal id)
        {
            return db.Genres.Count(e => e.GENREID == id) > 0;
        }
    }
}