﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/Bookings")]
    public class BookingsController : ApiController
    {
        private BookingModel db = new BookingModel();

        // GET: api/Bookings
        /// <summary>
        /// Get information for all booking
        /// </summary>
        /// <remarks>Returns a list of all bookings</remarks>
        /// <returns>Venue schema</returns>
        public IQueryable<Booking> GetBookings()
        {
            return db.Bookings;
        }

        // GET: api/Bookings/5
        /// <summary>
        /// Get information about a specific booking
        /// </summary>
        /// <remarks>Returns a specific booking</remarks>
        /// <param name="id">Booking ID</param>
        /// <returns>Booking schema</returns>
        [ResponseType(typeof(Booking))]
        public async Task<IHttpActionResult> GetBooking(decimal id)
        {
            Booking booking = await db.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }

            return Ok(booking);
        }

        // PUT: api/Bookings/5
        /// <summary>
        /// Update information for a specified booking
        /// </summary>
        /// <remarks>Modify information for a booking by a given ID</remarks>
        /// <param name="id">Booking ID</param>
        /// <param name="booking">Booking schema</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBooking(decimal id, Booking booking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != booking.BOOKINGID)
            {
                return BadRequest();
            }

            db.Entry(booking).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Bookings
        /// <summary>
        /// Add/Update information about a booking
        /// </summary>
        /// <remarks>Update information about a booking, or add if not exist</remarks>
        /// <param name="booking">Booking schema</param>
        /// <returns>Booking schema</returns>
        [ResponseType(typeof(Booking))]
        public async Task<IHttpActionResult> PostBooking(Booking booking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (booking.BOOKINGID == 0)
            {
                booking.BOOKINGID = db.Bookings.Count() + 1;
            }

            db.Bookings.Add(booking);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BookingExists(booking.BOOKINGID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = booking.BOOKINGID }, booking);
        }

        // DELETE: api/Bookings/5
        /// <summary>
        /// Delete specified booking
        /// </summary>
        /// <remarks>Delete the booking with the given ID</remarks>
        /// <param name="id">Booking ID to delete</param>
        /// <returns>Booking schema</returns>
        [ResponseType(typeof(Booking))]
        public async Task<IHttpActionResult> DeleteBooking(decimal id)
        {
            Booking booking = await db.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }

            db.Bookings.Remove(booking);
            await db.SaveChangesAsync();

            return Ok(booking);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BookingExists(decimal id)
        {
            return db.Bookings.Count(e => e.BOOKINGID == id) > 0;
        }
    }
}