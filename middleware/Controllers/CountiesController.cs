﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;

namespace middleware.Controllers
{
    /// <summary>
    /// Controller for a county
    /// </summary>
    [RoutePrefix("api/Counties")]
    public class CountiesController : ApiController
    {
        private CountyModel db = new CountyModel();

        // GET: api/Counties
        /// <summary>
        /// Get information for all counties
        /// </summary>
        /// <remarks>Returns a list of all counties</remarks>
        /// <returns>County Schema</returns>
        public IQueryable<County> GetCounties()
        {
            return db.Counties;
        }

        // GET: api/Counties/5
        /// <summary>
        /// Get information about a specific county
        /// </summary>
        /// <remarks>Returns a specific county</remarks>
        /// <param name="id">County ID</param>
        /// <returns>County schema</returns>
        [ResponseType(typeof(County))]
        public async Task<IHttpActionResult> GetCounty(decimal id)
        {
            County county = await db.Counties.FindAsync(id);
            if (county == null)
            {
                return NotFound();
            }

            return Ok(county);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CountyExists(decimal id)
        {
            return db.Counties.Count(e => e.COUNTYID == id) > 0;
        }
    }
}