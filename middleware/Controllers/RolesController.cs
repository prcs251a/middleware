﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/Roles")]
    public class RolesController : ApiController
    {
        private RoleModel db = new RoleModel();

        // GET: api/Roles
        /// <summary>
        /// Get information for all role
        /// </summary>
        /// <remarks>Returns a list of all roles</remarks>
        /// <returns>Role schema</returns>
        public IQueryable<Role> GetRoles()
        {
            return db.Roles;
        }

        // GET: api/Roles/5
        /// <summary>
        /// Get information about a specific role
        /// </summary>
        /// <remarks>Returns a specififc role</remarks>
        /// <param name="id">Role ID</param>
        /// <returns>Role schema</returns>
        [ResponseType(typeof(Role))]
        public async Task<IHttpActionResult> GetRole(decimal id)
        {
            Role role = await db.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }

            return Ok(role);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoleExists(decimal id)
        {
            return db.Roles.Count(e => e.ROLEID == id) > 0;
        }
    }
}