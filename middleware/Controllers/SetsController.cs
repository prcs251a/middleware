﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/Sets")]
    public class SetsController : ApiController
    {
        private SetModel db = new SetModel();

        // GET: api/Sets
        /// <summary>
        /// Get information for all sets
        /// </summary>
        /// <remarks>Returns a list of all sets</remarks>
        /// <returns>Set schema</returns>
        public IQueryable<Set> GetSets()
        {
            return db.Sets;
        }

        // GET: api/Sets/5
        /// <summary>
        /// Get information about a specific set
        /// </summary>
        /// <remarks>Returns a specific set</remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Set))]
        public async Task<IHttpActionResult> GetSet(decimal id)
        {
            Set set = await db.Sets.FindAsync(id);
            if (set == null)
            {
                return NotFound();
            }

            return Ok(set);
        }

        // PUT: api/Sets/5
        /// <summary>
        /// Update information for a specific set
        /// </summary>
        /// <remarks>Modify information for a set by a given ID</remarks>
        /// <param name="id"></param>
        /// <param name="set"></param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSet(decimal id, Set set)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != set.EVENTID)
            {
                return BadRequest();
            }

            db.Entry(set).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sets
        /// <summary>
        /// Add/Update information about a set
        /// </summary>
        /// <remarks>Update information about a set, or add if not exist</remarks>
        /// <param name="set">Set ID</param>
        /// <returns>Set schema</returns>
        [ResponseType(typeof(Set))]
        public async Task<IHttpActionResult> PostSet(Set set)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (set.ARTISTID == 0)
            {
                set.ARTISTID = db.Sets.Count() + 1;
            }

            db.Sets.Add(set);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SetExists(set.EVENTID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = set.EVENTID }, set);
        }

        // DELETE: api/Sets/5
        /// <summary>
        /// Delete specifiec set
        /// </summary>
        /// <remarks>Delete the set with a given ID</remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Set))]
        public async Task<IHttpActionResult> DeleteSet(decimal id)
        {
            Set set = await db.Sets.FindAsync(id);
            if (set == null)
            {
                return NotFound();
            }

            db.Sets.Remove(set);
            await db.SaveChangesAsync();

            return Ok(set);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SetExists(decimal id)
        {
            return db.Sets.Count(e => e.EVENTID == id) > 0;
        }
    }
}