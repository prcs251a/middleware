﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/customers/")]
    public class CustomersController : ApiController
    {
        private CustomerModel db = new CustomerModel();

        // GET: api/Customers
        /// <summary>
        /// Get information for all customers
        /// </summary>
        /// <remarks>Returns a list of all customers</remarks>
        /// <returns>Customer schema</returns>
        public IQueryable<Customer> GetCustomers()
        {
            return db.Customers;
        }

        // GET: api/Customers/5
        /// <summary>
        /// Get information about a specific customer
        /// </summary>
        /// <remarks>Returns a specific customer</remarks>
        /// <param name="id">Customer ID</param>
        /// <returns>Customer schema</returns>
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> GetCustomer(decimal id)
        {
            Customer customer = await db.Customers.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // PUT: api/Customers/5
        /// <summary>
        /// Update information for a specificed customer
        /// </summary>
        /// <remarks>Modify information for a customer by a given ID</remarks>
        /// <param name="id">Customer ID to update</param>
        /// <param name="customer">New customer schema</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCustomer(decimal id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customer.CUSTOMERID)
            {
                return BadRequest();
            }

            db.Entry(customer).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Customers
        /// <summary>
        /// Add/Update information about a customer
        /// </summary>
        /// <remarks>Update information about the customer, or add if not exist</remarks>
        /// <param name="customer">Customer schema</param>
        /// <returns>Customer schema</returns>
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (customer.CUSTOMERID == 0)
            {
                customer.CUSTOMERID = db.Customers.Count() + 1;
            }

            db.Customers.Add(customer);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CustomerExists(customer.CUSTOMERID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = customer.CUSTOMERID }, customer);
        }

        // DELETE: api/Customers/5
        /// <summary>
        /// Delete a specified customer
        /// </summary>
        /// <remarks>Delete the customer with the given ID</remarks>
        /// <param name="id">Customer ID</param>
        /// <returns>Customer schema</returns>
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> DeleteCustomer(decimal id)
        {
            Customer customer = await db.Customers.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            db.Customers.Remove(customer);
            await db.SaveChangesAsync();

            return Ok(customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(decimal id)
        {
            return db.Customers.Count(e => e.CUSTOMERID == id) > 0;
        }
    }
}