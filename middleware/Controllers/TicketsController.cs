﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;
using Newtonsoft.Json.Linq;

namespace middleware.Controllers
{
    [RoutePrefix("api/Tickets")]
    public class TicketsController : ApiController
    {
        private TicketModel db = new TicketModel();

        // GET: api/Tickets
        /// <summary>
        /// Get information for all tickets
        /// </summary>
        /// <remarks>Returns a list of all tickets</remarks>
        /// <returns>Ticket schema</returns>
        public IQueryable<Ticket> GetTickets()
        {
            return db.Tickets;
        }

        // GET: api/Tickets/5
        /// <summary>
        /// Get information about a specific ticket
        /// </summary>
        /// <remarks>Returns a specific ticket</remarks>
        /// <param name="id">Ticket ID</param>
        /// <returns>Ticket schema</returns>
        [ResponseType(typeof(Ticket))]
        public async Task<IHttpActionResult> GetTicket(decimal id)
        {
            Ticket ticket = await db.Tickets.FindAsync(id);
            if (ticket == null)
            {
                return NotFound();
            }

            return Ok(ticket);
        }

        // PUT: api/Tickets/5
        /// <summary>
        /// Update information about a ticket
        /// </summary>
        /// <remarks>Modify information for a ticket by a given ID</remarks>
        /// <param name="id">Ticket ID</param>
        /// <param name="ticket">Ticket schema</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTicket(decimal id, Ticket ticket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ticket.TICKETID)
            {
                return BadRequest();
            }

            db.Entry(ticket).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tickets
        /// <summary>
        /// Add/Update information about a ticket
        /// </summary>
        /// <remarks>Update information about a ticket, or add if not exist</remarks>
        /// <param name="ticket">Ticket ID</param>
        /// <returns>Ticket schema</returns>
        [ResponseType(typeof(Ticket))]
        public async Task<IHttpActionResult> PostTicket(Ticket ticket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ticket.TICKETID == 0)
            {
                ticket.TICKETID = db.Tickets.Count() + 1;
            }

            db.Tickets.Add(ticket);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TicketExists(ticket.TICKETID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = ticket.TICKETID }, ticket);
        }

        // DELETE: api/Tickets/5
        /// <summary>
        /// Delete a specified ticket
        /// </summary>
        /// <remarks>Delete the ticket with the given ID</remarks>
        /// <param name="id">Ticket ID</param>
        /// <returns>Ticket schema</returns>
        [ResponseType(typeof(Ticket))]
        public async Task<IHttpActionResult> DeleteTicket(decimal id)
        {
            Ticket ticket = await db.Tickets.FindAsync(id);
            if (ticket == null)
            {
                return NotFound();
            }

            db.Tickets.Remove(ticket);
            await db.SaveChangesAsync();

            return Ok(ticket);
        }

        [AcceptVerbs("GET")]
        [Route("queryByEvent")]
        public async Task<List<Ticket>> queryByEventId(decimal eventId, decimal bookingId = 0, decimal tierId = 0)
        {
            var eventsDb = new EventModel();
            var e = await eventsDb.Events.FindAsync(eventId);
            if (e == null)
            {
                return null;
            }

            var tickets = new List<Ticket>();

            if (bookingId == 0 && tierId == 0)
            {
                tickets = await db.Tickets.Where(ticket => ticket.EVENTID == eventId).ToListAsync();
            }

            else if (bookingId != 0)
            {
                tickets = await db.Tickets.Where(ticket => ticket.EVENTID == eventId && ticket.BOOKINGID == bookingId).ToListAsync();
            }

            else if (tierId != 0)
            {
                tickets = await db.Tickets.Where(ticket => ticket.EVENTID == eventId && ticket.BOOKINGID == bookingId && ticket.TIERID == tierId).ToListAsync();
            }

            return tickets;
        }

        [Route("purchaseTickets")]
        public async Task<IHttpActionResult> purchaseTickets()
        {
            var bookingsDb = new BookingModel();
            dynamic json = JObject.Parse(HttpContext.Current.Request.Form["json"]);
            var ticketList = new List<Ticket>();
            var bookingId = bookingsDb.Bookings.Max(b => b.BOOKINGID) + 1;
            var count = 0;
            foreach (var i in json.tickets)
            {
                ticketList.Add(new Ticket()
                {
                    BOOKINGID = bookingId,
                    EVENTID = i.EventID,
                    TICKETID = db.Tickets.Max(ticket => ticket.TICKETID) + (1 + count),
                    TIERID = i.TierID
                });

                count++;
            }

            var booking = new Booking()
            {
                BOOKINGID = bookingId,
                CUSTOMERID = json.data.CustomerID,
                DATEPURCHASED = DateTime.Now,
                TOTALCOST = json.data.Cost
            };
            bookingsDb.Bookings.Add(booking);

            db.Tickets.AddRange(ticketList);
            try
            {
                await bookingsDb.SaveChangesAsync();
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                throw e;
            }

            return Ok(bookingId);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketExists(decimal id)
        {
            return db.Tickets.Count(e => e.TICKETID == id) > 0;
        }
    }
}