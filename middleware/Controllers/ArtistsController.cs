﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;
using FuzzyString;

namespace middleware.Controllers
{
    /// <summary>
    /// Controller for an Artist
    /// </summary>
    [RoutePrefix("api/Artists")]
    public class ArtistsController : SearchableController<Artist>
    {
        private ArtistModel db = new ArtistModel();

        // GET: api/Artists
        /// <summary>
        /// Get information for all artist
        /// </summary>
        /// <remarks>Returns a list of all artists</remarks>
        /// <returns>Student List</returns>
        public IQueryable<Artist> GetArtists()
        {
            return db.Artists;
        }

        // GET: api/Artists/5
        /// <summary>
        /// Get information about a specific artist
        /// </summary>
        /// <param name="id">Artist ID</param>
        /// <remarks>Returns a specific artist</remarks>
        /// <returns>Specified Artist</returns>
        [ResponseType(typeof(Artist))]
        public async Task<IHttpActionResult> GetArtist(decimal id)
        {
            Artist artist = await db.Artists.FindAsync(id);
            if (artist == null)
            {
                return NotFound();
            }

            return Ok(artist);
        }

        // PUT: api/Artists/5
        /// <summary>
        /// Update information for a specified artist
        /// </summary>
        /// <remarks>Modify information for an artist by a given ID</remarks>
        /// <param name="id">Artist ID</param>
        /// <param name="artist">Artist schema</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArtist(decimal id, Artist artist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artist.ARTISTID)
            {
                return BadRequest();
            }

            db.Entry(artist).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtistExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Artists
        /// <summary>
        /// Add/Update information about an artist
        /// </summary>
        /// <remarks>Update information about the artist, or add if not exist</remarks>
        /// <param name="artist">Artist schema</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(Artist))]
        public async Task<IHttpActionResult> PostArtist(Artist artist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (artist.ARTISTID == 0)
            {
                artist.ARTISTID = db.Artists.Count() + 1;
            }

            db.Artists.Add(artist);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ArtistExists(artist.ARTISTID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = artist.ARTISTID }, artist);
        }

        // DELETE: api/Artists/5
        /// <summary>
        /// Delete specified artist
        /// </summary>
        /// <remarks>Delete the artist with the given ID</remarks>
        /// <param name="id">Artist ID</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(Artist))]
        public async Task<IHttpActionResult> DeleteArtist(decimal id)
        {
            Artist artist = await db.Artists.FindAsync(id);
            if (artist == null)
            {
                return NotFound();
            }

            db.Artists.Remove(artist);
            await db.SaveChangesAsync();

            return Ok(artist);
        }

        // POST: api/Artists/search
        /// <summary>
        /// Search for specific values by a string
        /// </summary>
        /// <remarks>Using a list of comma-seperated values, search for a given query string.</remarks>
        /// <param name="q">Query string to search for</param>
        /// <param name="types">Valid search types are:
        /// <para />
        /// "genre"
        /// <para />
        /// "name"
        /// </param>
        /// <returns></returns>
        [Route("search")]
        [AcceptVerbs("GET")]
        public IHttpActionResult Search(string q, string types = "name")
        {
            var values = ValidateTokens(new List<string>
            {
                "genre",
                "name"
            }, types);

            if (values == null)
            {
                return BadRequest("Error: query list not formatted properly.");
            }

            var resultMap = new Dictionary<String, List<Artist>>();

            foreach (var v in values)
            {
                switch (v)
                {
                    case "genre":
                        resultMap.Add("genre", SearchByField(db.Artists, q, "GENRE.GENRENAME"));
                        break;
                    case "name":
                        resultMap.Add("name", SearchByField(db.Artists, q, "ARTISTNAME"));
                        break;
                    default:
                        return BadRequest("Something went wrong parsing values.");
                }
            }

           return Ok(resultMap);
        }

        [Route("downloadImage")]
        [AcceptVerbs("GET")]
        public async Task<HttpResponseMessage> DownloadImage(decimal id, int width, int height, string type)
        {
            var artist = await db.Artists.FindAsync(id);
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            if (artist.IMAGE == null)
            {
                result.Content = null;
                return result;
            }

            switch (type.ToUpper())
            {
                case "B":
                    result.Content = new ByteArrayContent(artist.IMAGE.IMAGEBANNER);
                    break;
                case "T":
                    result.Content = new ByteArrayContent(artist.IMAGE.IMAGETHUMB);
                    break;
            }
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return result;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtistExists(decimal id)
        {
            return db.Artists.Count(e => e.ARTISTID == id) > 0;
        }
    }
}