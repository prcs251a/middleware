﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/Images")]
    public class ImageController : ApiController
    {
        private ImageModel db = new ImageModel();

        [Route("uploadImage")]
        public async Task<IHttpActionResult> uploadImage()
        {
            var provider = new MultipartMemoryStreamProvider();
            var img = new Image {IMAGEID = db.Images.Max(i => i.IMAGEID) + 1};
            var files = await Request.Content.ReadAsMultipartAsync(provider);

            foreach (var file in files.Contents)
            {
                var filename = new Regex("[^a-zA-Z0-9 -]").Replace(provider.Contents[files.Contents.IndexOf(file)].Headers.ContentDisposition.Name, "");
                if (filename == "banner")
                {
                    img.IMAGEBANNER = await file.ReadAsByteArrayAsync();
                }

                if (filename == "thumb")
                {
                    img.IMAGETHUMB = await file.ReadAsByteArrayAsync();
                }
            }
            
            img.IMAGENAME = HttpContext.Current.Request.Form["desc"];
            db.Images.Add(img);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = img.IMAGEID }, img);
        }
    }
}
