﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Web.Http;
using System.Web.Http.Description;
using FuzzyString;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/Users")]
    public class UsersController : SearchableController<User>
    {
        private UserModel db = new UserModel();

        // GET: api/Users
        /// <summary>
        /// Get information for all users.
        /// </summary>
        /// <returns>List of users</returns>
        public IQueryable<User> GetUsers()
        {
            return db.Users;
        }

        // GET: api/Users/5
        /// <summary>
        /// Get information for a specific user.
        /// </summary>
        /// <param name="id">User ID</param>
        /// <returns>Information for a specific user</returns>
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(decimal id)
        {
            User User = db.Users.Find(id);
            if (User == null)
            {
                return NotFound();
            }

            return Ok(User);
        }

        // PUT: api/Users/5
        /// <summary>
        /// Update information for a specific user
        /// </summary>
        /// <param name="id">User ID to update</param>
        /// <param name="user">New information</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(decimal id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.USERID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="user">User to create</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (user.USERID == 0)
            {
                user.USERID = db.Users.Count() + 1;
            }

            db.Users.Add(user);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.USERID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = user.USERID }, user);
        }

        // DELETE: api/Users/5
        /// <summary>
        /// Delete a user.
        /// </summary>
        /// <param name="id">User ID to delete</param>
        /// <returns>Status code</returns>
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(decimal id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        /// <summary>
        /// Search a user name by query
        /// </summary>
        /// <remarks>Returns a list of users that match the query</remarks>
        /// <param name="query">Query to search for</param>
        /// <returns>Search results</returns>
        [Route("search")]
        [AcceptVerbs("GET")]
        public List<User> QueryUsersByName(string q)
        {
            return SearchByField(db.Users, q, "USERNAME");
        }

        /// <summary>
        /// Get the role name of the users
        /// </summary>
        /// <remarks>Returns the role of the user by id</remarks>
        /// <param name="id">User ID</param>
        /// <returns>Role</returns>
        [Route("getRole")]
        public List<User> QueryUsersByRole(string query)
        {
            return db.Users.Where(user => user.ROLE.NAME.ToUpper().Contains(query.ToUpper())).ToList();
        }

        [Route("getTickets")]
        public List<Ticket> GetTickets(decimal userId)
        {
            var bookingModel = new BookingModel();
            var ticketModel = new TicketModel();
            var user = db.Users.Find(userId);
            var bookingList = bookingModel.Bookings.Where(b => b.CUSTOMERID == user.CUSTOMERID);
            var ticketList = new List<Ticket>();
            foreach (var booking in bookingList)
            {
                ticketList.AddRange(ticketModel.Tickets.Where(t => t.BOOKINGID == booking.BOOKINGID));
            }

            return ticketList;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(decimal id)
        {
            return db.Users.Count(e => e.USERID == id) > 0;
        }
    }
}