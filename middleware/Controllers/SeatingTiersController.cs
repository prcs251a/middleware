﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/SeatingTiers")]
    public class SeatingTiersController : ApiController
    {
        private SeatingTierModel db = new SeatingTierModel();

        // GET: api/SeatingTiers
        /// <summary>
        /// Get information for all seating tiers
        /// </summary>
        /// <remarks>Returns a list of all seating tiers.</remarks>
        /// <returns>Seating tier schema</returns>
        public IQueryable<SeatingTier> GetSeatingTiers()
        {
            return db.SeatingTiers;
        }

        // GET: api/SeatingTiers/5
        /// <summary>
        /// Get information about a specific seating tiers
        /// </summary>
        /// <remarks>Returns a specific seating tier.</remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(SeatingTier))]
        public async Task<IHttpActionResult> GetSeatingTier(decimal id)
        {
            SeatingTier seatingTier = await db.SeatingTiers.FindAsync(id);
            if (seatingTier == null)
            {
                return NotFound();
            }

            return Ok(seatingTier);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SeatingTierExists(decimal id)
        {
            return db.SeatingTiers.Count(e => e.TIERID == id) > 0;
        }
    }
}