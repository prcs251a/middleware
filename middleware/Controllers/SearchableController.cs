﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FuzzyString;
using System.Reflection;

namespace middleware.Controllers
{
    public class SearchableController<T> : ApiController where T : class
    {
        /// <summary>
        /// Options for fuzzy finder
        /// </summary>
        protected List<FuzzyStringComparisonOptions> options = new List<FuzzyStringComparisonOptions>
        {
            FuzzyStringComparisonOptions.UseOverlapCoefficient,
            FuzzyStringComparisonOptions.UseLongestCommonSubsequence,
            FuzzyStringComparisonOptions.UseLongestCommonSubstring
        };

        /// <summary>
        /// How close strings to match to be accepted
        /// </summary>
        protected const FuzzyStringComparisonTolerance tolerance = FuzzyStringComparisonTolerance.Strong;

        /// <summary>
        /// Fuzzy search in model for q, limited by field.
        /// </summary>
        /// <param name="model">Model to search in</param>
        /// <param name="q">Query to search for</param>
        /// <param name="field">Field within the model to limit search by</param>
        /// <returns>List of results</returns>
        protected List<T> SearchByField(DbSet<T> model, String q, String field)
        {
            List<T> resultList = new List<T>();

            foreach (var m in model)
            {
                var isCancelled = m.GetType().GetProperty("ISCANCELLED");
                if (isCancelled?.GetValue(m, null) != null && isCancelled.ToString().Equals("1"))
                    break;
                var prop = GetPropValue(m, field);

                if (prop.ToString().ApproximatelyEquals(q, options, tolerance))
                {
                    resultList.Add(m);
                }
            }

            return resultList;
        }

        /// <summary>
        /// Ensure that CSV matches any/all search categories
        /// </summary>
        /// <param name="tokenList">List of valid tokens</param>
        /// <param name="types">CSV passed to API</param>
        /// <returns>List of matches if valid, or null</returns>
        protected List<string> ValidateTokens(List<string> tokenList, string types)
        {
            var values = types.Split(',').ToList();
            return values.Any(v => !tokenList.Contains(v)) ? null : values;
        }

        /// <summary>
        /// Reflect an object recursively. 
        /// Borrowed from http://mcgivery.com/c-reflection-get-property-value-of-nested-classes/
        /// </summary>
        /// <param name="obj">Object to reflect</param>
        /// <param name="propName">Field to find</param>
        /// <returns>The field if found, or null</returns>
        private Object GetPropValue(Object obj, String propName)
        {
            var nameParts = propName.Split('.');
            if (nameParts.Length == 1)
            {
                return obj.GetType().GetProperty(propName).GetValue(obj, null);
            }

            foreach (var part in nameParts)
            {
                if (obj == null) { return null; }

                var type = obj.GetType();
                var info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }
    }
}
