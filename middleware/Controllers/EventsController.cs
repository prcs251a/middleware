﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using middleware.Models;
using System.Globalization;
using FuzzyString;
using middleware.exceptions;
using Microsoft.Ajax.Utilities;

namespace middleware.Controllers
{
   [RoutePrefix("api/Events")] 
   public class EventsController : SearchableController<Event>
    {
        private EventModel db = new EventModel();

        // GET: api/Events
        /// <summary>
        /// Get information for all event
        /// </summary>
        /// <remarks>Returns a list of all events</remarks>
        /// <returns>Event schema</returns>
        public IQueryable<Event> GetEvents()
        {
            return db.Events.Where(e => !e.ISCANCELLED);
        }

        // GET: api/Events/5
        /// <summary>
        /// Get information about a specific event
        /// </summary>
        /// <remarks>Returns a specific event</remarks>
        /// <param name="id">Event ID</param>
        /// <returns>Event schema</returns>
        [ResponseType(typeof(Event))]
        public async Task<IHttpActionResult> GetEvent(decimal id)
        {
            Event e = await db.Events.FindAsync(id);
            if (e == null)
            {
                return NotFound();
            }

            if (e.ISCANCELLED)
            {
                return BadRequest($"'{e.TOUR.TOURNAME}' at '{e.VENUE.VENUENAME}' is cancelled.");
            }

            return Ok(e);
        }

        // PUT: api/Events/5
        /// <summary>
        /// Update information about a specific event
        /// </summary>
        /// <remarks>Modify information for an event by a given ID</remarks>
        /// <param name="id">Event ID</param>
        /// <param name="e">Event schema</param>
        /// <returns>Event schema</returns>
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEvent(decimal id, Event e)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != e.EVENTID)
            {
                return BadRequest();
            }

            Event ev = await db.Events.FindAsync(id);
            if (ev.ISCANCELLED != e.ISCANCELLED)
            {
                throw new CantModifyIsCancelledException();
            }

            db.Entry(e).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Events
        /// <summary>
        /// Add/Update information about an event
        /// </summary>
        /// <remarks>Update information about an event, or add if not exist</remarks>
        /// <param name="e">Event scema</param>
        /// <returns></returns>
        [ResponseType(typeof(Event))]
        public async Task<IHttpActionResult> PostEvent(Event e)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (e.EVENTID == 0)
            {
                e.EVENTID = db.Events.Count() + 1;
            }

            db.Events.Add(e);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EventExists(e.EVENTID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = e.EVENTID }, e);
        }

        // DELETE: api/Events/5
        /// <summary>
        /// Delete specific event
        /// </summary>
        /// <remarks>Delete event with the given ID</remarks>
        /// <param name="id">Event ID</param>
        /// <returns>Event schema</returns>
        [ResponseType(typeof(Event))]
        public async Task<IHttpActionResult> DeleteEvent(decimal id)
        {
            Event e = await db.Events.FindAsync(id);
            if (e == null)
            {
                return NotFound();
            }

            if (!e.ISCANCELLED)
            {
                throw new NotCancelledException($"'{e.TOUR.TOURNAME}' at '{e.VENUE.VENUENAME}'");
            }

            db.Events.Remove(e);
            await db.SaveChangesAsync();

            return Ok(e);
        }

        /// <summary>
        /// Search for specific values by a string.
        /// </summary>
        /// <remarks>Using a list of comma-seperated values, search for a given query string.
        /// Valid terms are:
        /// "date", "genre", "artist", "venue", "tour". Date cannot be used with any of the others, and will error as such.
        /// </remarks>
        /// <param name="q">Query string to search for</param>
        /// <param name="types">Search types</param>
        /// <returns></returns>
        [Route("search")]
        [AcceptVerbs("GET")]
        public IHttpActionResult Search(string q, string types = "date")
        {
            var values = ValidateTokens(new List<string>
            {
                "date",
                "genre",
                "artist",
                "venue",
                "tour"
            }, types);

            if (values == null)
            {
                return BadRequest("Query list not formatted properly.");
            }

            var throwaway = new DateTime();
            var resultMap = new Dictionary<String, List<Event>>();

            if (values.Contains("date") && !DateTime.TryParse(q, out throwaway))
            {
                return BadRequest($"Requested 'date' but didn't send a proper DateTime type. ({q})");
            }

            if (!values.Contains("date") && DateTime.TryParse(q, out throwaway))
            {
                return BadRequest($"Requested '{values}' but sent a DateTime type. ({q})");
            }

            foreach (var v in values)
            {
                switch (v)
                {
                    case "date":
                        var finalList = db.Events.Where(e => e.STARTTIME.ToString("yyyy-MM-dd").Equals(DateTime.Parse(q).ToString("yyyy-MM-dd")) && !e.ISCANCELLED).ToList();
                        resultMap.Add("date", finalList);
                        break;

                    case "genre":
                        resultMap.Add("genre", SearchByField(db.Events, q, "TOUR.ARTIST.GENRE.GENRENAME"));
                        break;

                    case "artist":
                        resultMap.Add("artist", SearchByField(db.Events, q, "TOUR.ARTIST.ARTISTNAME"));
                        break;

                    case "venue":
                        resultMap.Add("venue", SearchByField(db.Events, q, "VENUE.VENUENAME"));
                        break;

                    case "tour":
                        resultMap.Add("tour", SearchByField(db.Events, q, "TOUR.TOURNAME"));
                        break;

                    default:
                        return BadRequest("Something went wrong parsing values");
                }
            }

            return Ok(resultMap);
        }

        [Route("cancel")]
        public async Task<IHttpActionResult> Cancel(decimal eventid)
        {
            Event e = await db.Events.FindAsync(eventid);
            if (e == null)
            {
                return NotFound();
            }

            if (e.ISCANCELLED)
            {
                throw new CancelledException($"'{e.TOUR.TOURNAME}' at '{e.VENUE.VENUENAME}'");
            }

            e.ISCANCELLED = true;
            await db.SaveChangesAsync();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventExists(decimal id)
        {
            return db.Events.Count(e => e.EVENTID == id) > 0;
        }
    }
}