﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuzzyString;
using middleware.exceptions;
using middleware.Models;

namespace middleware.Controllers
{
    [RoutePrefix("api/Tours")]
    public class ToursController : SearchableController<Tour>
    {
        private TourModel db = new TourModel();

        // GET: api/Tours
        /// <summary>
        /// Get information for all tours
        /// </summary>
        /// <remarks>Returns a list of all tours</remarks>
        /// <returns>Tour schema</returns>
        public IQueryable<Tour> GetTours()
        {
            return db.Tours.Where(t => !t.ISCANCELLED);
        }

        // GET: api/Tours/5
        /// <summary>
        /// Get information about a specific tour
        /// </summary>
        /// <remarks>Returns a specific user</remarks>
        /// <param name="id">Tour ID</param>
        /// <returns>Tour schema</returns>
        [ResponseType(typeof(Tour))]
        public async Task<IHttpActionResult> GetTour(decimal id)
        {
            Tour tour = await db.Tours.FindAsync(id);
            if (tour == null)
            {
                return NotFound();
            }

            if (tour.ISCANCELLED)
            {
                throw new CancelledException($"'{tour.TOURNAME}'");
            }

            return Ok(tour);
        }

        // PUT: api/Tours/5
        /// <summary>
        /// Update information about a specific tour
        /// </summary>
        /// <remarks>Modify information for a tour by a given ID</remarks>
        /// <param name="id">Tour ID</param>
        /// <param name="tour">Tour schema</param>
        /// <returns>Tour schema</returns>
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTour(decimal id, Tour tour)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tour.TOURID)
            {
                return BadRequest();
            }

            Tour t = await db.Tours.FindAsync(id);
            db.Entry(tour).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TourExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tours
        /// <summary>
        /// Add/Update information for a specified tour
        /// </summary>
        /// <remarks>Modify information for a tour by a given ID</remarks>
        /// <param name="tour">Tour ID</param>
        /// <returns>Tour schema</returns>
        [ResponseType(typeof(Tour))]
        public async Task<IHttpActionResult> PostTour(Tour tour)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (tour.TOURID == 0)
            {
                tour.TOURID = db.Tours.Count() + 1;
            }

            db.Tours.Add(tour);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TourExists(tour.TOURID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tour.TOURID }, tour);
        }

        // DELETE: api/Tours/5
        /// <summary>
        /// Delete a specific tour
        /// </summary>
        /// <remarks>Delete the tour with the given ID</remarks>
        /// <param name="id">Tour ID</param>
        /// <returns>Tour schema</returns>
        [ResponseType(typeof(Tour))]
        public async Task<IHttpActionResult> DeleteTour(decimal id)
        {
            Tour tour = await db.Tours.FindAsync(id);
            if (tour == null)
            {
                return NotFound();
            }

            if (!tour.ISCANCELLED)
            {
                throw new NotCancelledException($"{tour.TOURNAME}");
            }

            db.Tours.Remove(tour);
            await db.SaveChangesAsync();

            return Ok(tour);
        }

        // POST: api/Tours/search
        /// <summary>
        /// Search tours based on a specified query
        /// </summary>
        /// <remarks>Searches tour name for the passed query</remarks>
        /// <param name="query">Query to search for</param>
        /// <returns>Search results</returns>
        [Route("search")]
        [AcceptVerbs("GET")]
        public List<Tour> SearchTours(string q)
        {
            return SearchByField(db.Tours, q, "TOURNAME");
        }

        // POST: api/Tours/findByArtist/5
        /// <summary>
        /// Get all tours for a given artist ID
        /// </summary>
        /// <remarks>Returns a list of all tours with the given ID</remarks>
        /// <param name="id">Artist ID to find</param>
        /// <returns>Search results</returns>
        [Route("findByArtist")]
        public List<Tour> findByArtist(decimal id)
        {
            return db.Tours.Where(tour => tour.ARTISTID.Equals(id) && !tour.ISCANCELLED).ToList();
        }

        /// <summary>
        /// Mark a tour as cancelled
        /// </summary>
        /// <remarks>Cancel a tour, or error if it's already cancelled</remarks>
        /// <param name="tourid">Tour to cancel</param>
        /// <returns>Result</returns>
        [Route("cancel")]
        public async Task<IHttpActionResult> Cancel(decimal tourid)
        {
            Tour tour = await db.Tours.FindAsync(tourid);
            if (tour == null)
            {
                return NotFound();
            }

            if (tour.ISCANCELLED)
            {
                throw new CancelledException($"'{tour.TOURNAME}'");
            }

            tour.ISCANCELLED = true;
            await db.SaveChangesAsync();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TourExists(decimal id)
        {
            return db.Tours.Count(e => e.TOURID == id) > 0;
        }
    }
}