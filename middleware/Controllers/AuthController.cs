﻿using middleware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json;

namespace middleware.Controllers
{
    [RoutePrefix("api/Auth")]
    public class AuthController : ApiController
    {
        private UserModel userDb = new UserModel();
        private CustomerModel customerDb = new CustomerModel();
        /// <summary>
        /// Attempt to login as provided user
        /// </summary>
        /// <param name="username">Username of the user</param>
        /// <param name="password">Password of the user</param>
        /// <returns>Result</returns>
        [Route("login")]
        public IHttpActionResult Login(string username, string password)
        {
            var status = false;
            decimal id = 0;

            foreach (var i in userDb.Users)
            {
                if (!i.USERNAME.Equals(username) || !i.PASSWORD.Equals(password)) continue;
                status = true;
                id = i.USERID;
                break;
            }
            return status ? (IHttpActionResult)Ok(id) : (IHttpActionResult)NotFound();
        }

        [Route("register")]
        public async Task<IHttpActionResult> Register(string name, string username, string email, string password, string postcode, int county, string city, string line1, string line2 = null)
        {
            if (Enumerable.Any(userDb.Users, user => user.USERNAME.Equals(username.ToUpper())) 
                || Enumerable.Any(userDb.Users, user => user.EMAIL.Equals(email.ToUpper()))
                || Enumerable.Any(customerDb.Customers, customer => customer.CUSTOMEREMAIL.Equals(email.ToUpper())))
            {
                return Conflict();
            }

            var names = name.Split(' ');

            userDb.Users.Add(new User()
            {
                EMAIL = email,
                PASSWORD = password,
                USERNAME = username,
                USERID = userDb.Users.Max(user => user.USERID) + 1,
                ROLEID = 3
            });
            var Customer = new Customer()
            {
                CUSTOMERFIRSTNAME = names[0],
                CUSTOMERLASTNAME = names[1],
                CUSTOMEREMAIL = email,
                ADDRESSLINE1 = line1,
                ADDRESSLINE2 = line2,
                CITY = city,
                COUNTYID = county,
                POSTCODE = postcode,
                CUSTOMERID = customerDb.Customers.Max(customer => customer.CUSTOMERID) + 1
            };
            customerDb.Customers.Add(Customer);
            
            await userDb.SaveChangesAsync();
            await customerDb.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = Customer.CUSTOMERID }, Customer);
        }
    }
}
