namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a genre.
    /// </summary>
    [Table("PRCS251A.GENRES")]
    public partial class Genre
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public decimal GENREID { get; set; }

        /// <summary>
        /// Name of the genre
        /// </summary>
        [StringLength(50)]
        public string GENRENAME { get; set; }
    }
}
