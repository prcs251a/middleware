namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a user.
    /// </summary>
    public partial class UserModel : DbContext
    {
        public UserModel()
            : base("name=UserDB")
        {
        }

        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>()
                .Property(e => e.ROLEID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Role>()
                .Property(e => e.NAME)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.USERID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<User>()
                .Property(e => e.USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.CUSTOMERID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<User>()
                .Property(e => e.ROLEID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<User>()
                .Property(e => e.EMAIL)
                .IsUnicode(false);
        }
    }
}
