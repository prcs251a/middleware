namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a customer.
    /// </summary>
    public partial class CustomerModel : DbContext
    {
        public CustomerModel()
            : base("name=CustomerModel")
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .Property(e => e.CUSTOMERID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Customer>()
                .Property(e => e.CUSTOMERFIRSTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.CUSTOMERLASTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.CUSTOMEREMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.COUNTYID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Customer>()
                .Property(e => e.CITY)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.ADDRESSLINE1)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.ADDRESSLINE2)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.POSTCODE);
        }
    }
}
