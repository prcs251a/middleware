namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a county.
    /// </summary>
    [Table("PRCS251A.COUNTIES")]
    public partial class County
    {
        /// <summary>
        /// Unique identifier for the county
        /// </summary>
        public decimal COUNTYID { get; set; }

        /// <summary>
        /// Name of the county
        /// </summary>
        [StringLength(40)]
        public string COUNTYNAME { get; set; }
    }
}
