namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251A.IMAGES")]
    public partial class Image
    {
        [Key]
        public decimal IMAGEID { get; set; }

        [Required]
        [StringLength(25)]
        public string IMAGENAME { get; set; }

        [Required]
        public byte[] IMAGEBANNER { get; set; }

        [Required]
        public byte[] IMAGETHUMB { get; set; }
    }
}
