﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace middleware.Models
{
    public class ImageModel : DbContext
    {
        public ImageModel()
            : base("name=ImageModel")
        {
        }

        public virtual DbSet<Image> Images { get; set; }
       
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Image>()
                .Property(e => e.IMAGEID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Image>()
                .Property(e => e.IMAGENAME)
                .IsUnicode(false);
        }
    }
}