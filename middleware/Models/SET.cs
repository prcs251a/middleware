namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a set.
    /// </summary>
    [Table("PRCS251A.SETS")]
    public partial class Set
    {
        /// <summary>
        /// Unique identifier for the Event
        /// </summary>
        [Key]
        [Column(Order = 0)]
        public decimal EVENTID { get; set; }

        /// <summary>
        /// Unique identifier for the Artist
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public decimal ARTISTID { get; set; }
    }
}
