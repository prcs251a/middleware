namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a tour.
    /// </summary>
    [Table("PRCS251A.TOURS")]
    public partial class Tour
    {
        /// <summary>
        /// Unique identifier for the tour
        /// </summary>
        public decimal TOURID { get; set; }

        /// <summary>
        /// Name of the tour
        /// </summary>
        [Required]
        [StringLength(100)]
        public string TOURNAME { get; set; }

        /// <summary>
        /// Start date for the tour
        /// </summary>
        public DateTime STARTDATE { get; set; }

        /// <summary>
        /// End date for the tour
        /// </summary>
        public DateTime ENDDATE { get; set; }

        /// <summary>
        /// Unique identifier for the artist
        /// </summary>
        public decimal ARTISTID { get; set; }

        /// <summary>
        /// Description of the tour
        /// </summary>
        [StringLength(1000)]
        public string TOURDESCRIPTION { get; set; }

        /// <summary>
        /// Virtual accessor for artists
        /// </summary>
        public virtual Artist ARTIST { get; set; }

        /// <summary>
        /// Flag to check if tour is cancelled
        /// </summary>
        public bool ISCANCELLED { get; set; }
    }
}
