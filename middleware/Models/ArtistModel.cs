namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for an artist.
    /// </summary>
    public partial class ArtistModel : DbContext
    {
        public ArtistModel()
            : base("name=ArtistModel")
        {
        }

        public virtual DbSet<Artist> Artists { get; set; }

        public virtual DbSet<Genre> Genres { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>()
                .Property(e => e.GENREID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Genre>()
                .Property(e => e.GENRENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Artist>()
                .Property(e => e.ARTISTID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Artist>()
                .Property(e => e.ARTISTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Artist>()
                .Property(e => e.ARTISTDESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<Artist>()
                .Property(e => e.GENREID)
                .HasPrecision(38, 0);
        }
    }
}
