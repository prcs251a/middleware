namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a booking.
    /// </summary>
    [Table("PRCS251A.BOOKINGS")]
    public partial class Booking
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public decimal BOOKINGID { get; set; }

        /// <summary>
        /// Unique identifier for the customer who made the booking
        /// </summary>
        public decimal CUSTOMERID { get; set; }

        /// <summary>
        /// Date of purchase
        /// </summary>
        public DateTime DATEPURCHASED { get; set; }

        /// <summary>
        /// Cost of the ticket including booking fee
        /// </summary>
        public decimal TOTALCOST { get; set; }
    }
}
