namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a seating tier.
    /// </summary>
    public partial class SeatingTierModel : DbContext
    {
        public SeatingTierModel()
            : base("name=SeatingTierModel")
        {
        }

        public virtual DbSet<SeatingTier> SeatingTiers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SeatingTier>()
                .Property(e => e.TIERID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<SeatingTier>()
                .Property(e => e.TICKETPRICE)
                .HasPrecision(38, 0);

            modelBuilder.Entity<SeatingTier>()
                .Property(e => e.TIERNAME)
                .IsUnicode(false);
        }
    }
}
