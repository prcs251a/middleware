namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a venue.
    /// </summary>
    [Table("PRCS251A.VENUES")]
    public partial class Venue
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public decimal VENUEID { get; set; }

        /// <summary>
        /// Name of the venue
        /// </summary>
        [Required]
        [StringLength(100)]
        public string VENUENAME { get; set; }

        /// <summary>
        /// Description of the venue
        /// </summary>
        [StringLength(1000)]
        public string VENUEDESCRIPTION { get; set; }

        /// <summary>
        /// Total tickets available for standing
        /// </summary>
        public decimal? STANDINGCAPACITY { get; set; }

        /// <summary>
        /// Total tickets available for tier 1
        /// </summary>
        public decimal? FIRSTTIERCAPACITY { get; set; }

        /// <summary>
        /// Total tickets available for tier 2
        /// </summary>
        public decimal? SECONDTIERCAPACITY { get; set; }

        /// <summary>
        /// Unique identifier for the county the venue is located in
        /// </summary>
        public decimal COUNTYID { get; set; }

        /// <summary>
        /// City the venue is located in
        /// </summary>
        [StringLength(100)]
        public string CITY { get; set; }

        /// <summary>
        /// Name of venue (not sure about this)
        /// </summary>
        [StringLength(300)]
        public string ADDRESSLINE1 { get; set; }

        /// <summary>
        /// Street number and name the venue is located in
        /// </summary>
        [StringLength(300)]
        public string ADDRESSLINE2 { get; set; }

        /// <summary>
        /// Postcode the venue is located in
        /// </summary>
        [Required]
        [StringLength(10)]
        public string POSTCODE { get; set; }

        /// <summary>
        /// Telephone number for the venue
        /// </summary>
        [Required]
        [StringLength(20)]
        public string TELEPHONENUMBER { get; set; }

        /// <summary>
        /// Unique identifier for image
        /// </summary>
        public decimal? IMAGEID { get; set; }

        /// <summary>
        /// Image tied to the entity
        /// </summary>
        public virtual Image IMAGE { get; set; }
    }
}
