namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a ticket.
    /// </summary>
    [Table("PRCS251A.TICKETS")]
    public partial class Ticket
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public decimal TICKETID { get; set; }

        /// <summary>
        /// Unique identifier for the tier
        /// </summary>
        public decimal TIERID { get; set; }

        /// <summary>
        /// Unique identifier for the booking
        /// </summary>
        public decimal BOOKINGID { get; set; }

        /// <summary>
        /// Unique identifier for the event
        /// </summary>
        public decimal EVENTID { get; set; }
    }
}
