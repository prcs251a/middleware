namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a tour.
    /// </summary>
    public partial class TourModel : DbContext
    {
        public TourModel()
            : base("name=TourModel")
        {
        }

        public virtual DbSet<Tour> Tours { get; set; }
        public virtual DbSet<Artist> Artists { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tour>()
                .Property(e => e.TOURID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Tour>()
                .Property(e => e.TOURNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Tour>()
                .Property(e => e.ARTISTID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Tour>()
                .Property(e => e.TOURDESCRIPTION)
                .IsUnicode(false);
        }
    }
}
