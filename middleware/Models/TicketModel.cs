namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a ticket.
    /// </summary>
    public partial class TicketModel : DbContext
    {
        public TicketModel()
            : base("name=TicketModel")
        {
        }

        public virtual DbSet<Ticket> Tickets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ticket>()
                .Property(e => e.TICKETID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Ticket>()
                .Property(e => e.TIERID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Ticket>()
                .Property(e => e.BOOKINGID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Ticket>()
                .Property(e => e.EVENTID)
                .HasPrecision(38, 0);
        }
    }
}
