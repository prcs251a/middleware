namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a venue.
    /// </summary>
    public partial class VenueModel : DbContext
    {
        public VenueModel()
            : base("name=VenueModel")
        {
        }

        public virtual DbSet<Venue> Venues { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venue>()
                .Property(e => e.VENUEID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Venue>()
                .Property(e => e.VENUENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Venue>()
                .Property(e => e.VENUEDESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<Venue>()
                .Property(e => e.STANDINGCAPACITY)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Venue>()
                .Property(e => e.FIRSTTIERCAPACITY)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Venue>()
                .Property(e => e.SECONDTIERCAPACITY)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Venue>()
                .Property(e => e.COUNTYID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Venue>()
                .Property(e => e.CITY)
                .IsUnicode(false);

            modelBuilder.Entity<Venue>()
                .Property(e => e.ADDRESSLINE1)
                .IsUnicode(false);

            modelBuilder.Entity<Venue>()
                .Property(e => e.ADDRESSLINE2)
                .IsUnicode(false);

            modelBuilder.Entity<Venue>()
                .Property(e => e.POSTCODE)
                .IsUnicode(false);

            modelBuilder.Entity<Venue>()
                .Property(e => e.TELEPHONENUMBER)
                .IsUnicode(false);
        }
    }
}
