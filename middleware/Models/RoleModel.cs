namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a role.
    /// </summary>
    public partial class RoleModel : DbContext
    {
        public RoleModel()
            : base("name=RoleModel")
        {
        }

        public virtual DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>()
                .Property(e => e.ROLEID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Role>()
                .Property(e => e.NAME)
                .IsUnicode(false);
        }
    }
}
