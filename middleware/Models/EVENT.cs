namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for an event.
    /// </summary>
    [Table("PRCS251A.EVENTS")]
    public partial class Event
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public decimal EVENTID { get; set; }

        /// <summary>
        /// Date of the event
        /// </summary>
        public DateTime EVENTDATE { get; set; }

        /// <summary>
        /// Start time of the event
        /// </summary>
        public DateTime STARTTIME { get; set; }

        /// <summary>
        /// End time of the event
        /// </summary>
        public DateTime? ENDTIME { get; set; }

        /// <summary>
        /// Tickets available for standing tier
        /// </summary>
        public decimal? STANDINGTICKETS { get; set; }

        /// <summary>
        /// Tickets available for tier 1
        /// </summary>
        public decimal? FIRSTTIERTICKETS { get; set; }

        /// <summary>
        /// Tickets available for tier 2
        /// </summary>
        public decimal? SECONDTIERTICKETS { get; set; }

        /// <summary>
        /// Unique identifier for the venue the event is located in
        /// </summary>
        public decimal VENUEID { get; set; }

        /// <summary>
        /// Unique identifier for the tour the event occurs in
        /// </summary>
        public decimal TOURID { get; set; }

        /// <summary>
        /// Virtual accessor for tours
        /// </summary>
        public virtual Tour TOUR { get; set; }

        /// <summary>
        /// Virtual accessor for venues
        /// </summary>
        public virtual Venue VENUE { get; set; }

        /// <summary>
        /// Flag to check if event is cancelled
        /// </summary>
        public bool ISCANCELLED { get; set; }
    }
}
