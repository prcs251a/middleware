namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a user.
    /// </summary>
    [Table("PRCS251A.USERS")]
    public partial class User
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public decimal USERID { get; set; }

        /// <summary>
        /// User-specified string to identify the user and to login with
        /// </summary>
        [Required]
        [StringLength(100)]
        public string USERNAME { get; set; }

        /// <summary>
        /// (Temporary)
        /// </summary>
        [Required]
        public string PASSWORD { get; set; }

        /// <summary>
        /// Unique identifier only applicable is the user is a customer
        /// </summary>
        public decimal? CUSTOMERID { get; set; }

        /// <summary>
        /// Role of the user
        /// </summary>
        public decimal ROLEID { get; set; }

        /// <summary>
        /// Email address used for correspondance or to login with
        /// </summary>
        [Required]
        [StringLength(100)]
        public string EMAIL { get; set; }

        /// <summary>
        /// Role of the user
        /// </summary>
        public virtual Role ROLE { get; set; }
    }
}
