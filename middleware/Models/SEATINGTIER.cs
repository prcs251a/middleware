namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a seating tier.
    /// </summary>
    [Table("PRCS251A.SEATINGTIERS")]
    public partial class SeatingTier
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        [Key]
        public decimal TIERID { get; set; }

        /// <summary>
        /// Price of the ticket
        /// </summary>
        public decimal TICKETPRICE { get; set; }

        /// <summary>
        /// Name of the tier ticket is for
        /// </summary>
        [Required]
        [StringLength(100)]
        public string TIERNAME { get; set; }
    }
}
