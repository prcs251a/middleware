namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a role.
    /// </summary>
    [Table("PRCS251A.ROLES")]
    public partial class Role
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        [Key]
        public decimal ROLEID { get; set; }

        /// <summary>
        /// Name of the role
        /// </summary>
        [Required]
        [StringLength(16)]
        public string NAME { get; set; }
    }
}
