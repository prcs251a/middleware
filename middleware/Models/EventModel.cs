namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for an event.
    /// </summary>
    public partial class EventModel : DbContext
    {
        public EventModel()
            : base("name=EventModel")
        {
        }

        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Tour> Tours { get; set; }
        public virtual DbSet<Venue> Venues { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Event>()
                .Property(e => e.EVENTID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Event>()
                .Property(e => e.STANDINGTICKETS)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Event>()
                .Property(e => e.FIRSTTIERTICKETS)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Event>()
                .Property(e => e.SECONDTIERTICKETS)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Event>()
                .Property(e => e.VENUEID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Event>()
                .Property(e => e.TOURID)
                .HasPrecision(38, 0);
        }
    }
}
