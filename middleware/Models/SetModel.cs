namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a set.
    /// </summary>
    public partial class SetModel : DbContext
    {
        public SetModel()
            : base("name=SetModel")
        {
        }

        public virtual DbSet<Set> Sets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Set>()
                .Property(e => e.EVENTID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Set>()
                .Property(e => e.ARTISTID)
                .HasPrecision(38, 0);
        }
    }
}
