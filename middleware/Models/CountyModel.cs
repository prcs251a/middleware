namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a county.
    /// </summary>
    public partial class CountyModel : DbContext
    {
        public CountyModel()
            : base("name=CountyModel")
        {
        }

        public virtual DbSet<County> Counties { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<County>()
                .Property(e => e.COUNTYID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<County>()
                .Property(e => e.COUNTYNAME)
                .IsUnicode(false);
        }
    }
}
