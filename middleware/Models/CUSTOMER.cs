namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for a customer.
    /// </summary>
    [Table("PRCS251A.CUSTOMERS")]
    public partial class Customer
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public decimal CUSTOMERID { get; set; }

        /// <summary>
        /// First name of the customer
        /// </summary>
        [Required]
        [StringLength(50)]
        public string CUSTOMERFIRSTNAME { get; set; }

        /// <summary>
        /// Last name of the customer
        /// </summary>
        [StringLength(50)]
        public string CUSTOMERLASTNAME { get; set; }

        /// <summary>
        /// Email address of the customer (possibly redundant)
        /// </summary>
        [Required]
        [StringLength(50)]
        public string CUSTOMEREMAIL { get; set; }

        /// <summary>
        /// Unique identifier for the county the customer is located in
        /// </summary>
        public decimal COUNTYID { get; set; }

        /// <summary>
        /// Name of the city the customer is located in
        /// </summary>
        [StringLength(100)]
        public string CITY { get; set; }

        /// <summary>
        /// House name or number the customer is located in
        /// </summary>
        [Required]
        [StringLength(40)]
        public string ADDRESSLINE1 { get; set; }

        /// <summary>
        /// Street number and name the customer is located in
        /// </summary>
        [StringLength(40)]
        public string ADDRESSLINE2 { get; set; }

        /// <summary>
        /// Postcode the customer is located at
        /// </summary>
        public string POSTCODE { get; set; }
    }
}
