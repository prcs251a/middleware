namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a booking.
    /// </summary>
    public partial class BookingModel : DbContext
    {
        public BookingModel()
            : base("name=BookingModel")
        {
        }

        public virtual DbSet<Booking> Bookings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Booking>()
                .Property(e => e.BOOKINGID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Booking>()
                .Property(e => e.CUSTOMERID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Booking>()
                .Property(e => e.TOTALCOST)
                .HasPrecision(38, 0);
        }
    }
}
