namespace middleware.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Model for a genre.
    /// </summary>
    public partial class GenreModel : DbContext
    {
        public GenreModel()
            : base("name=GenreModel")
        {
        }

        public virtual DbSet<Genre> Genres { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>()
                .Property(e => e.GENREID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<Genre>()
                .Property(e => e.GENRENAME)
                .IsUnicode(false);
        }
    }
}
