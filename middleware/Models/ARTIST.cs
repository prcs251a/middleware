using System.Web;

namespace middleware.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Data structure for an artist.
    /// </summary>
    [Table("PRCS251A.ARTISTS")]
    public partial class Artist
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public decimal ARTISTID { get; set; }

        /// <summary>
        /// Name of the artist
        /// </summary>
        [StringLength(100)]
        public string ARTISTNAME { get; set; }

        /// <summary>
        /// Description of the artist
        /// </summary>
        [StringLength(1000)]
        public string ARTISTDESCRIPTION { get; set; }

        /// <summary>
        /// Unique identifier for the genre of the artist
        /// </summary>
        public decimal? GENREID { get; set; }

        /// <summary>
        /// Unique identifier for the image
        /// </summary>
        public decimal? IMAGEID { get; set; }

        /// <summary>
        /// Genre of the artist
        /// </summary>
        public virtual Genre GENRE {get; set; }

        /// <summary>
        /// Image tied to the entity
        /// </summary>
        public virtual Image IMAGE { get; set; }
    }
}
