﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace middleware.exceptions
{
    /// <summary>
    /// Exception to handle attempts to delete entities that aren't cancelled
    /// </summary>
    public class NotCancelledException : Exception
    { 
            /// <summary>
            /// Base ctor
            /// </summary>
            public NotCancelledException() { }

            /// <summary>
            /// Descriptive message
            /// </summary>
            /// <param name="message">Message to append</param>
            public NotCancelledException(string message) : base(message + " is still valid.") { }

            /// <param name="message">Message to append</param>
            /// <param name="inner">Inner exception</param>
            public NotCancelledException(string message, Exception inner) : base(message + " is still valid.", inner) { }

            /// <summary>
            /// Serializable ctor
            /// </summary>
            /// <param name="info"></param>
            /// <param name="ctx"></param>
            protected NotCancelledException(SerializationInfo info, StreamingContext ctx) : base(info, ctx) { }
    }
}