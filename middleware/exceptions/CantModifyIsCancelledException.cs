﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace middleware.exceptions
{
    public class CantModifyIsCancelledException : Exception
    {
        /// <summary>
        /// Base ctor
        /// </summary>
        public CantModifyIsCancelledException() : base("Can't modify ISCANCELLED") {}

        /// <summary>
        /// Serializable ctor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="ctx"></param>
        protected CantModifyIsCancelledException(SerializationInfo info, StreamingContext ctx) : base(info, ctx) { }
    }
}