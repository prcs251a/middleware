﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace middleware.exceptions
{
    /// <summary>
    /// Exception for entities that are attempting to be cancelled while already cancelled
    /// </summary>
    public class CancelledException : Exception
    {
        /// <summary>
        /// Base ctor
        /// </summary>
        public CancelledException() {}

        /// <summary>
        /// Descriptive message of what is cancelled
        /// </summary>
        /// <param name="message">Message to append</param>
        public CancelledException(string message) : base(message + " is cancelled.") {}
        
        /// <param name="message">Message to append</param>
        /// <param name="inner">Inner exception</param>
        public CancelledException(string message, Exception inner) : base(message + " is cancelled.", inner) {}

        /// <summary>
        /// Serializable ctor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="ctx"></param>
        protected CancelledException(SerializationInfo info, StreamingContext ctx) : base(info, ctx) {}
    }
}